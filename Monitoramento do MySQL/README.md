**Monitoramento do MySQL**

**my.cnf**

**- Criar arquivo na pasta /etc/zabbix**

        [mysql]
        user=monitdb
        password=123456
        [mysqladmin]
        user=monitdb
        password=123456
        
**- Acessar console do MySQL e dar acesso aos databasesque serão monitorados**

        mysql> GRANT USAGE ON *.* TO monitdb@localhostIDENTIFIED BY '123456' WITH GRANT OPTION;

**userparameters_mysql.conf**

**- Baixar arquivo para o diretório /etc/zabbix/zabbix_agentd.conf.d/**

        # cd/etc/zabbix/zabbix_agentd.conf.d/
        # wget https://raw.githubusercontent.com/janssenlima/scripts-zabbix/master/conf/userparameters_mysql.conf

**Configurar zabbix_agentd.conf**

        ### Option: Include
        Include=/etc/zabbix/zabbix_agentd.conf.d/

**- Reinicie o serviço**

**Testes com zabbix_get**

        # zabbix_get-s 127.0.0.1 -k mysql.status[Com_insert]
        # zabbix_get-s 127.0.0.1 -k mysql.version
        # zabbix_get-s 127.0.0.1 -k mysql.ping
        # zabbix_get-s 127.0.0.1 -k mysql.size
        # zabbix_get-s 127.0.0.1 -k mysql.size[zabbix]
        # zabbix_get-s 127.0.0.1 -k mysql.size[zabbix,hosts]
        # zabbix_get-s 127.0.0.1 -k mysql.size[zabbix,history,index]
        # zabbix_get-s 127.0.0.1 -k mysql.size[zabbix,history,data]
        # zabbix_get-s 127.0.0.1 -k mysql.size[zabbix,,data]

**Template AppMySQL**

**- Associar o template ao host que executa o servidor MySQL**

**Observações**

**Caso for realizar o monitoramento através de um host externo, o arquivo de configuração com os comandos SQL devem ser modificados.**

**Basta incluir o parâmetro -h <ip-servidor>**